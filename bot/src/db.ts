import {default as Datastore} from "nedb";

export const db = {
    groups: new Datastore('data/db/groups.db'),
    playlists: new Datastore('data/db/playlists.db')
};

export function dbInit() {
    db.groups.loadDatabase();
    db.playlists.loadDatabase();
}