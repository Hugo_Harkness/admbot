import {DMChannel, GuildMember, MessageEmbed, MessageReaction, TextChannel} from "discord.js";

const emojies = ["✅","❌"]; //
const emojiesNum = ["0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣"];
const filter = (reaction: MessageReaction) => {
    return emojies.includes(reaction.emoji.name);
};
const filterNum = (reaction: MessageReaction) => {
    return emojiesNum.includes(reaction.emoji.name);
};

export async function Confirmation(description: string, title: string, channel: TextChannel,
                                   member: GuildMember, callb: (result: boolean) => void, timeout = 20){
    let embed = new MessageEmbed()
    embed.setTitle(title);
    embed.setDescription(description);

    let msg = await channel.send(embed);
    await msg.react(emojies[0]);
    await msg.react(emojies[1]);

    let confirmed = false;

    const collector = msg.createReactionCollector(filter, { time: timeout*1000 });

    collector.on('collect',(collected) => {
        if (collected.count && collected.count <= 1) {return;}
        collected.users.cache.forEach(user => {
            if(user.id == member.id){
                if(collected.emoji.name == emojies[0])
                    confirmed = true;
                collector.stop();
            }
        })
    });

    collector.on('end', () => {
        msg.delete();
        callb(confirmed);
    });
}

export async function Vote(description: string, title: string, channel: TextChannel, callb: (result: any) => void, timeout = 10) {
    let embed = new MessageEmbed()
    embed.setTitle(title);
    embed.setDescription(description);

    let msg = await channel.send(embed);
    await msg.react(emojies[0]);
    await msg.react(emojies[1]);

    const collector = msg.createReactionCollector(filter, { time: timeout*1000 });

    collector.on('end', (collecteds) => {
        msg.delete();
        let result = {
            yes : 0,
            no : 0
        };
        for (const collected of collecteds.array()) {
            if(collected.emoji.name == emojies[0])
                result.yes += 1;
            else if (collected.emoji.name == emojies[1])
                result.no +=1;
        }
        callb(result);
    });
}

export async function QCM(description: string, title: string, channel: TextChannel, member: GuildMember, callb: null | ((result: number) => void) = null, timeout = 60) : Promise<number>{
    let embed = new MessageEmbed()
    embed.setTitle(title);
    embed.setDescription(description);

    let msg = await channel.send(embed);
    await msg.react(emojiesNum[0]);
    await msg.react(emojiesNum[1]);
    await msg.react(emojiesNum[2]);
    await msg.react(emojiesNum[3]);
    await msg.react(emojiesNum[4]);

    let result = -1;

    const collector = msg.createReactionCollector(filterNum, { time: timeout*1000 });

    collector.on('collect',(collected) => {
        if (collected.count && collected.count <= 1) {return;}
        collected.users.cache.forEach(user => {
            if(user.id == member.id){
                result = emojiesNum.indexOf(collected.emoji.name);
                collector.stop();
            }
        })
    });

    return new Promise((resolve) => {
        collector.on('end', () => {
            msg.delete();
            if(callb)
                callb(result);
            else
                resolve(result);
        });
    });
}