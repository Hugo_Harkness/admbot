var fs = require('fs');
var util = require('util');
var log_file = fs.createWriteStream('./data/debug.log', { flags: 'a' });

export default function log(error: number, message: string, origin: string, srv: string) {
    const date_ob = new Date;
    // adjust 0 before single digit date
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let hours = ("0" + date_ob.getHours()).slice(-2);
    let minutes = ("0" + date_ob.getMinutes()).slice(-2);

    let output = `${date}/${month} ${hours}:${minutes} [${origin}|${srv}] : ${message}`;

    if (error == 1)
        output = `\x1b[32m${output}\x1b[0m`;
    else if (error == 2)
        output = `\x1b[33m${output}\x1b[0m`;
    else if (error == 3)
        output = `\x1b[31m${output}\x1b[0m`;

    console.log(output);
    log_file.write(util.format(output) + '\n');
};