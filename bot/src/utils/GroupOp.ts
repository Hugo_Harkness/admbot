import { GuildMember } from 'discord.js';
import { db } from '../db';
import { CommandContext } from '../commands/commandContext';

export function joinGrp(grp: any, member: GuildMember, cc: CommandContext){
    let grpRole = cc.guild.roles.cache.get(grp.roleID);
    if(!grpRole) return;
    member.roles.add(grpRole);
    db.groups.update({ _id: grp._id }, { $addToSet: { members: member.id } });
};

export function adminGrp(grp: any, member: GuildMember, cc: CommandContext){
    let grpCat = cc.guild.channels.cache.get(grp.catID);
    if(!grpCat) return;
    grpCat.updateOverwrite(member, { MANAGE_CHANNELS: true });
    db.groups.update({ _id: grp._id }, { $addToSet: { admin: member.id } });
};

export function leaveGrp(grp: any, member: GuildMember, cc: CommandContext){
    let grpRole = cc.guild.roles.cache.get(grp.roleID);
    if(!grpRole) return;
    let grpCat = cc.guild.channels.cache.get(grp.catID);
    if(!grpCat) return;
    member.roles.remove(grpRole);
    grpCat.updateOverwrite(member, { MANAGE_CHANNELS: false , VIEW_CHANNEL: false});
    db.groups.update({ _id: grp._id }, { $pull: { members: member.id } });
    db.groups.update({ _id: grp._id }, { $pull: { admin: member.id } });
};