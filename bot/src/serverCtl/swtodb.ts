import {default as Datastore} from "nedb";

const db = {
    groups: new Datastore('db/groups.db')
};
db.groups.loadDatabase();


import { Client, Guild, GuildMember, PartialGuildMember } from 'discord.js';
import { readFileSync } from "fs";
import log from "../utils/log"; 

const filOri = __filename.slice(__dirname.length + 1);
const client = new Client();

let guildG: Guild;
let mod: string;

function ready(client : Client) {
    client.user?.setActivity('!!help', { type: 'WATCHING' });
    log(0, `Logged in as ${client.user?.tag} with id: ${client.user?.id} on:`, filOri,"");
    client.guilds.cache.forEach(guild => {
        log(0, `-- ${guild.name}  id: ${guild.id} owner: ${guild.owner?.displayName} owner.id: ${guild.owner?.id}`, filOri,"");
    });

    client.guilds.cache.forEach((guild) => {
        guild.channels.cache.forEach((chan) => {
            if(chan.type == "category" && !chan.name.endsWith("lobal")) {
                let name = chan.name.substr(4);
                let prv = (chan.name.startsWith("prv-"));
                let role = guild.roles.cache.find(r => r.name.toUpperCase() == (`grp-${name}`).toUpperCase() || r.name.toUpperCase() == (`prv-${name}`).toUpperCase());
                let members = guild.members.cache.filter(m => m.roles.cache.has(role?.id as string));
                let memberIDs = members.map((m) => m.id);
                let adminID = "389432812266192906";
                if(prv) {
                    let orole = guild.roles.cache.find(r => r.name == `o-${chan.name}`);
                    let adminTPMid = guild.members.cache.find(m => m.roles.cache.has(orole?.id as string))?.id;
                    if(adminTPMid) adminID = adminTPMid;

                    role?.setName(`grp-${name}`)
                }

                if(role)
                db.groups.insert({
                    name: name,
                    guildID: guild.id,
                    roleID: role.id,
                    catID: chan.id,
                    private: prv,
                    members: memberIDs,
                    admin: [adminID]
                });
                console.log({
                    name: name,
                    guildID: guild.name,
                    roleID: role?.name,
                    catID: chan.name,
                    private: prv,
                    members: members.map(m => m.user.username),
                    admin: [adminID]
                });
            
            }
        })
    });
};


client.on("ready", () => {ready(client)});

let token = readFileSync("token").toString().trim()
client.login(token);