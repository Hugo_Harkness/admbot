import { Client } from 'discord.js';
import { readFileSync } from "fs";
import log from "../utils/log"; 

const filOri = __filename.slice(__dirname.length + 1);
const client = new Client();

function ready(client : Client) {
    client.user?.setActivity('!!help', { type: 'WATCHING' });
    log(0, `Logged in as ${client.user?.tag} with id: ${client.user?.id} on:`, filOri,"");
    client.guilds.cache.forEach(guild => {
        log(0, `-- ${guild.name}  id: ${guild.id} owner: ${guild.owner?.displayName} owner.id: ${guild.owner?.id}`, filOri,"");
    });


    client.guilds.cache.get(process.argv[2])?.delete();
};


client.on("ready", () => {ready(client)});

let token = readFileSync("token").toString().trim()
client.login(token);