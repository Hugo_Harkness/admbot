import { Client, Guild, Permissions, GuildMember, PartialGuildMember } from 'discord.js';
import { readFileSync } from "fs";
import log from "../utils/log";

const filOri = __filename.slice(__dirname.length + 1);
const client = new Client();
let tmp: string, guest: string, member: string, mod: string;

let guildG: Guild;

function ready(client: Client) {
    client.user?.setActivity('!!help', { type: 'WATCHING' });
    log(0, `Logged in as ${client.user?.tag} with id: ${client.user?.id} on:`, filOri,"");
    client.guilds.cache.forEach(guild => {
        log(0, `-- ${guild.name}  id: ${guild.id} owner: ${guild.owner?.displayName} owner.id: ${guild.owner?.id}`, filOri,"");
    });

    guildG = (client.guilds.cache.get(process.argv[2]) as Guild);
    init((client.guilds.cache.get(process.argv[2]) as Guild)).then(() => {
        (client.guilds.cache.get(process.argv[2]) as Guild).members.fetch("389432812266192906").then(async usr => {
            await usr.roles.add(mod)
            await usr.roles.add(member)
            await usr.roles.add(guest)
        });
    })
}

export async function init(guild: Guild): Promise<string> {
    await clear(guild);

    await addBaseRole(guild);

    await addBaseChannels(guild);

    await guild.setDefaultMessageNotifications("MENTIONS");

    return mod;
}

async function clear(guild: Guild) {
    await guild.channels.cache.map(async channel => {
        await channel.delete();
    });
    log(1, `channels deleted`, filOri,"");

    guild.roles.cache.forEach((role) => {
        try {
            if (role.id === guild.id)
                return;
            role.delete();
        } catch (error) {
            console.log(error);
        }
    });
    log(1, `roles deleted`, filOri,"");
}

async function addBaseRole(guild: Guild) {
    await guild.roles.create({
        data: {
            name: "adm",
            hoist: true,
            color: "#ff0000",
            permissions: ["ADMINISTRATOR"]
        },
        reason: "Base role."
    });
    await guild.roles.create({
        data: {
            name: "master_key",
            hoist: true,
            color: "#ff0000",
            permissions: []
        },
        reason: "Base role."
    });
    await guild.roles.create({
        data: {
            name: "mod",
            hoist: true,
            color: "#ffcf00",
            permissions: ["MOVE_MEMBERS", "MANAGE_MESSAGES", "VIEW_AUDIT_LOG"]
        },
        reason: "Base role."
    }).then(role => mod = role.id);
    await guild.roles.create({
        data: {
            name: "member",
            hoist: true,
            color: "#00a5ff",
            permissions: ["MENTION_EVERYONE"]
        },
        reason: "Base role."
    }).then(role => member = role.id);
    await guild.roles.create({
        data: {
            name: "guest",
            hoist: true,
            color: "#d7d7d7",
            permissions: ["ADD_REACTIONS", "STREAM", "VIEW_CHANNEL", "SEND_MESSAGES", "EMBED_LINKS", "ATTACH_FILES", "USE_EXTERNAL_EMOJIS", "CONNECT", "SPEAK", "USE_VAD", "CHANGE_NICKNAME",
                "CREATE_INSTANT_INVITE", "MANAGE_EMOJIS", "READ_MESSAGE_HISTORY"]
        },
        reason: "Base role."
    }).then(role => guest = role.id);
    await guild.roles.create({
        data: {
            name: "tmp",
            hoist: true,
            permissions: ["ADD_REACTIONS", "STREAM", "VIEW_CHANNEL", "SEND_MESSAGES", "EMBED_LINKS", "ATTACH_FILES", "USE_EXTERNAL_EMOJIS", "CONNECT", "SPEAK", "USE_VAD", "CHANGE_NICKNAME"]
        },
        reason: "Base role."
    }).then(role => tmp = role.id);
    await guild.roles.fetch(guild.id).then(r => r?.setPermissions([]));
}

async function addBaseChannels(guild: Guild) {
    await guild.channels.create("Global", { type: "category" })
        .then(category => {
            guild.channels.create("info", {
                type: "text",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions(["VIEW_CHANNEL", "SEND_MESSAGES"]) }, { id: tmp, allow: new Permissions("VIEW_CHANNEL") }, { id: guest, allow: new Permissions("VIEW_CHANNEL") }, { id: mod, allow: new Permissions(["VIEW_CHANNEL", "SEND_MESSAGES"]) }]
            })
            guild.channels.create("general", {
                type: "text",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: tmp, allow: new Permissions("VIEW_CHANNEL") }, { id: guest, allow: new Permissions("VIEW_CHANNEL") }]
            })
                .then(channel => channel.createInvite().then(invite => console.log(invite.url)));
            guild.channels.create("member", {
                type: "text",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: member, allow: new Permissions("VIEW_CHANNEL") }]
            });
            guild.channels.create("bot", {
                type: "text",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("SEND_MESSAGES") }, { id: guest, allow: new Permissions("SEND_MESSAGES") }]
            });
            guild.channels.create("no mic", {
                type: "text",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: tmp, allow: new Permissions("VIEW_CHANNEL") }, { id: guest, allow: new Permissions("VIEW_CHANNEL") }]
            })
            guild.channels.create("general", {
                type: "voice",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: tmp, allow: new Permissions("VIEW_CHANNEL") }, { id: guest, allow: new Permissions("VIEW_CHANNEL") }]
            })
            guild.channels.create("general'", {
                type: "voice",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: tmp, allow: new Permissions("VIEW_CHANNEL") }, { id: guest, allow: new Permissions("VIEW_CHANNEL") }]
            })
            guild.channels.create("afk", {
                type: "voice",
                parent: category,
                permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: tmp, allow: new Permissions("VIEW_CHANNEL") }, { id: guest, allow: new Permissions("VIEW_CHANNEL") }]
            })
                .then(channel => guild.setAFKChannel(channel));
        });
}

client.on("ready", () => { ready(client) });

function addMod(guild: Guild, gMemb: GuildMember | PartialGuildMember) {
    if (!guild.members.cache.has(gMemb.id)) return;
    gMemb.roles.add(mod);
}

client.on("guildMemberAdd", (gmemb) => { addMod(guildG, gmemb) })

let token = readFileSync("data/token").toString().trim()
client.login(token).then(() => {});