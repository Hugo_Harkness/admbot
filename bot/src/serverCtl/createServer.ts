import { Client, Guild, GuildMember, PartialGuildMember } from 'discord.js';
import { readFileSync } from "fs";
import log from "../utils/log"; 
import { init } from './initSrv';

const filOri = __filename.slice(__dirname.length + 1);
const client = new Client();

let guildG: Guild;
let mod: string;

function ready(client : Client) {
    client.user?.setActivity('!!help', { type: 'WATCHING' });
    log(0, `Logged in as ${client.user?.tag} with id: ${client.user?.id} on:`, filOri,"");
    client.guilds.cache.forEach(guild => {
        log(0, `-- ${guild.name}  id: ${guild.id} owner: ${guild.owner?.displayName} owner.id: ${guild.owner?.id}`, filOri,"");
    });

    client.guilds.create(process.argv[2], {
        region: "europe",
        icon: null,
    }).then(guild => {
        guildG = guild;
        init(guild).then(modl => mod = modl);
    });
};

function addMod(guild: Guild, gMemb: GuildMember | PartialGuildMember) {
    if(!guild.members.cache.has(gMemb.id)) return;
    gMemb.roles.add(mod);
}

client.on("ready", () => {ready(client)});
client.on("guildMemberAdd", (gmemb) => {addMod(guildG,gmemb)})

let token = readFileSync("data/token").toString().trim()
client.login(token);