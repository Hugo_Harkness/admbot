import {Song} from "../../commands/music/song";
import {Guild, StreamDispatcher, VoiceChannel, VoiceConnection} from "discord.js";
import ytdl from "ytdl-core";

interface StreamData {
    queue: Array<Song>,
    now: Song,
    isPlaying: boolean,
    dispatcher: StreamDispatcher | undefined,
    connection: VoiceConnection | undefined,
}

export class StreamCtrl {
    private static GuildStreamData = new Map<String, StreamData>();

    init(guilds: Array<Guild>) {
        for (const guild of guilds) {
            StreamCtrl.GuildStreamData.set(guild.id, {
                queue: [],
                now: {isAvailable: false, title: "", url: "", volume: 0},
                isPlaying: false,
                dispatcher: undefined,
                connection: undefined,
            });
        }
    }

    getQueue(guildId: string): Array<Song> | undefined {
        return StreamCtrl.GuildStreamData.get(guildId)?.queue;
    }

    setQueue(value: Array<Song>, guildId: string) {
        let streamData = StreamCtrl.GuildStreamData.get(guildId);
        if (streamData)
            streamData.queue = value;
    }

    getNow(guildId: string): Song | undefined {
        return StreamCtrl.GuildStreamData.get(guildId)?.now;
    }

    setNow(value: Song, guildId: string) {
        let streamData = StreamCtrl.GuildStreamData.get(guildId);
        if (streamData)
            streamData.now = value;
    }

    getIsPlaying(guildId: string): boolean | undefined {
        return StreamCtrl.GuildStreamData.get(guildId)?.isPlaying;
    }

    setIsPlaying(value: boolean, guildId: string) {
        let streamData = StreamCtrl.GuildStreamData.get(guildId);
        if (streamData)
            streamData.isPlaying = value;
    }

    getDispatcher(guildId: string): StreamDispatcher | undefined {
        return StreamCtrl.GuildStreamData.get(guildId)?.dispatcher;
    }

    setDispatcher(value: StreamDispatcher | undefined, guildId: string) {
        let streamData = StreamCtrl.GuildStreamData.get(guildId);
        if (streamData)
            streamData.dispatcher = value;
    }

    getConnection(guildId: string): VoiceConnection | undefined {
        return StreamCtrl.GuildStreamData.get(guildId)?.connection;
    }

    setConnection(value: VoiceConnection | undefined, guildId: string) {
        let streamData = StreamCtrl.GuildStreamData.get(guildId);
        if (streamData)
            streamData.connection = value;
    }

    async connect(voiceChannel: VoiceChannel, guildId: string) {
        const connection = await voiceChannel.join();
        await connection.voice.setSelfDeaf(true);
        this.setConnection(connection, guildId);
        return connection;
    }

    addFormats(guildId: string){
        const queue = this.getQueue(guildId);
        if (queue) {
            for (const song of queue) {
                 StreamCtrl.addFormat(song).then(() => {});
            }
        }
    }

    private static async addFormat(song: Song){
        if(!song.format) {
            const info = await ytdl.getInfo(song.url)
            song.format = ytdl.chooseFormat(info.formats, {filter: "audio"}/*{quality: "highestaudio", filter: "audioonly"}*/);
        }
    }

    async play(voiceChannel: VoiceChannel, guildId: string): Promise<void> {
        if (this.getQueue(guildId)?.length == 0) {
            this.setIsPlaying(false, guildId);
            this.setConnection(undefined, guildId);
            this.setNow({isAvailable: false, title: "", url: "", volume: 0}, guildId);
            voiceChannel.leave();
            return;
        }

        const song = this.getQueue(guildId)?.shift();
        if (!song) return;

        this.setNow(song, guildId);
        if (!song.isAvailable) return this.play(voiceChannel, guildId);

        await StreamCtrl.addFormat(song);

        let connection = this.getConnection(guildId);
        if (!connection)
            connection = await this.connect(voiceChannel, guildId);

        const dispatcher = connection.play(ytdl(song.url, {highWaterMark: 1024 * 1024 * 10, format: song.format}), {
            volume: song.volume,
            highWaterMark: 40,
            fec: true,
            plp: 100,
            bitrate: 64
        });
        dispatcher.on('finish', () => {
            this.play(voiceChannel, guildId);
        });
        this.setDispatcher(dispatcher, guildId);

    }

    skip(guildId: string) {
        const dispatcher = this.getDispatcher(guildId);
        //console.log(dispatcher);
        if (dispatcher)
            dispatcher.end();
    }

    stop(guildId: string) {
        this.setQueue([], guildId);
        this.skip(guildId);
    }

    pauseToggle(guildId: string) {
        const dispatcher = this.getDispatcher(guildId);
        console.log(dispatcher?.paused);
        if (dispatcher)
            if (dispatcher.paused)
                dispatcher.resume();
            else
                dispatcher.pause();
    }


}