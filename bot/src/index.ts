import { Client, Message } from 'discord.js';
import { readFileSync } from "fs";
import { dbInit } from './db'

dbInit();

import { CommandHandler } from './commands/commandHandler'
import { ready } from './events/ready';
import { newTmp } from './events/newTpm';
import {StreamCtrl} from "./data/music/StreamCtrl";

const client = new Client();
const streamCtrl = new StreamCtrl();

//Events :
client.on("ready", () => {ready(client, streamCtrl)});
client.on('guildMemberAdd', (gm) => {newTmp(client, gm)})
client.on("message", (message: Message) => new CommandHandler("!!", streamCtrl).handleMessage(message));

let token = readFileSync("data/token").toString().trim()
client.login(token);