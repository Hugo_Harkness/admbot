import { Client } from "discord.js";
import log from "../utils/log";
import {StreamCtrl} from "../data/music/StreamCtrl";

export function ready(client : Client, streamCtrl: StreamCtrl) {
    client.user?.setActivity('!!help', { type: 'WATCHING' });
    log(0, `Logged in as ${client.user?.tag} with id: ${client.user?.id} on:`, "ready", "all");
    client.guilds.cache.forEach(guild => {
        log(0, `-- ${guild.name}  id: ${guild.id} owner: ${guild.owner?.displayName} owner.id: ${guild.owner?.id}`, "ready", guild.name);
    });

    //resete tmp
    client.guilds.cache.forEach(guild => {
        let roles = new Map<string, string>(guild.roles.cache.map(r => [r.name, r.id]));
        guild.members.cache.filter(gm => gm.roles.cache.has(roles.get("tmp") as string)).map(gm => {
            gm.kick("Server reboot.").then(() => {
                log(2, `${gm.nickname} kicked : tmp and srv reboot.`, "ready", guild.name);
            })
        });
    })

    streamCtrl.init(client.guilds.cache.array());
}