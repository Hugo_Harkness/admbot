import { Client, GuildMember, PartialGuildMember } from "discord.js";
import log from "../utils/log";

export function newTmp(client : Client, member: GuildMember | PartialGuildMember) {
    let roles = new Map<string, string>(member.guild.roles.cache.map(r => [r.name, r.id]));
    member.roles.add(roles.get("tmp") as string);
    log(1, `${member.user?.tag} joined.`, "newTmp", member.guild.name);
    client.setTimeout(() => {
        if(member.roles.cache.has(roles.get("tmp") as string)){
            member.kick("12 hour delay expired.");
            log(2, `${member.user?.tag} kicked : 12 hour delay expired.`, "newTmp", member.guild.name);
        }
    }, 12 * 60 * 60 * 1000);
};