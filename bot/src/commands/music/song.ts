import {videoFormat} from "ytdl-core";

export interface Song {
    url: string,
    title: string,
    volume: number,
    isAvailable: boolean,
    format?: videoFormat
}