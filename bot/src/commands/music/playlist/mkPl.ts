import {Command} from '../../command';
import {CommandContext} from '../../commandContext';
import {GuildMember} from 'discord.js';
import ytdl from 'ytdl-core';
import {db} from "../../../db";
import {Song} from "../song";
import search, {YouTubeSearchOptions} from "youtube-search";
import {readFileSync} from "fs";
import {QCM} from "../../../utils/dialog";

const opts: YouTubeSearchOptions = {
    maxResults: 10,
    key: readFileSync("data/apiGoogle").toString().trim()
};

export class MkPl implements Command {
    commandNames = ["mkpl", "make-playlist"];
    group = "Music:";

    async run(cc: CommandContext): Promise<void> {
        const name = cc.args.shift();
        const songs : Array<Song> = await MkPl.getSong(cc);
        //TODO: check unique
        if(name)
            MkPl.dbInsert(cc.guildID, cc.member, name, songs);
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }
    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }

    private static async getSong(cc: CommandContext): Promise<Array<Song>> {
        let songs = [];
        for (let url of cc.args) {
            if(!ytdl.validateURL(url)) {
                let results = (await search(url, opts)).results;
                results = results.filter(r => r.kind == "youtube#video")
                results = results.slice(0,5);
                let msg = "";
                results.forEach((item, index) => {
                    msg += `${index} : ${item.title}\n`;
                });
                let result = await QCM(msg, "YouTube Search:", cc.textChannel, cc.member);
                url = results[result].link;
            }
            const info = await ytdl.getInfo(url).catch(_reason => {});
            if(info)
                 songs.push({
                    isAvailable: true,
                    title: info.videoDetails.title,
                    url: url,
                    volume: 0.5
                });
        }
        return songs;
    }


    private static dbInsert(guildID: string, admin: GuildMember, name: string, songs: Array<Song>) {
        db.playlists.insert({
            guildID: guildID,
            admins: [admin.id],
            name: name,
            songs: songs,
        });
    }

}