import {Command} from "../../command";
import {CommandContext} from "../../commandContext";
import {db} from "../../../db";
import {MessageEmbed} from "discord.js";
import {Song} from "../song";

export class LsPl implements Command {
    commandNames = ["lspl", "list-playlist"];
    group = "Music:";

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }

    async run(cc: CommandContext): Promise<void> {
        const name = cc.args[0];
        if(name){
            let embed = new MessageEmbed()
            embed.setTitle(`${name}:`);
            db.playlists.findOne({guildID: cc.guildID, name: name}, (err, playlist) => {
                if(!playlist || err) return;
                let songs = "";
                playlist.songs.forEach((song: Song, index: number) => {
                    songs += `${index} : ${(!song.isAvailable) ? ":x:" : ""} ${song.title}\n`;
                })
                embed.setDescription(songs);

                const author = cc.guild.members.cache.filter((GM, key) => playlist.admins.indexOf(key) > -1);
                let authorList = "by ";
                author.forEach(author => {
                    authorList += `${author.displayName}; `;
                })
                embed.setFooter(authorList)
                cc.message.reply(embed).then(msg => {msg.delete({timeout:60*1000})});
            })
        } else {
            let embed = new MessageEmbed()
            embed.setTitle("Playlist:");
            let playlistsDesc = "";
            db.playlists.find({guildID: cc.guildID}, async (err: any, playlists: Array<any>) => {
                if(!playlists || err) return;
                playlists.forEach(playlist => { //TODO: guildID: cc.guildID,
                    const author = cc.guild.members.cache.get(playlist.admins[0]);
                    if (author)
                        playlistsDesc += `${playlist.name} : ${author.displayName} (${playlist.songs.length} songs)\n`;
                })
                embed.setDescription(playlistsDesc);
                await cc.message.reply(embed).then(msg => {msg.delete({timeout:60*1000})});
            })
        }
    }
}