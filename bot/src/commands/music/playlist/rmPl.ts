import {CommandContext} from "../../commandContext";
import {Confirmation} from "../../../utils/dialog";
import {Command} from "../../command";
import {db} from "../../../db";


export class RmPl implements Command {
    commandNames = ["rmpl", "remove-playlist"];
    group = "Music:";

    async run(cc: CommandContext): Promise<void> {
        const name = cc.args.shift();
        db.playlists.findOne({guildID: cc.guildID, name: name}, async (err, playlist) => {
            if(!playlist || err) return; //TODO: MSG err
            if(playlist.admins.indexOf(cc.member.id) == -1) return; //TODO: MSG err
            await Confirmation(`:bangbang: **${name}** will be deleted :bangbang:`, "Playlist Deletion:", cc.textChannel, cc.member, confirmed => {
                if(confirmed)
                    db.playlists.remove({_id: playlist._id});
            });
        });


    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }
    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }

}