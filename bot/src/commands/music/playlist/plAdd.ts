import {Command} from '../../command';
import {CommandContext} from '../../commandContext';
import ytdl from 'ytdl-core';
import {db} from "../../../db";
import {Song} from "../song";
import search, {YouTubeSearchOptions} from "youtube-search";
import {QCM} from "../../../utils/dialog";
import {readFileSync} from "fs";

const opts: YouTubeSearchOptions = {
    maxResults: 10,
    key: readFileSync("data/apiGoogle").toString().trim()
};

export class PlAdd implements Command {
    commandNames = ["pladd", "add-to-playlist"];
    group = "Music:";

    async run(cc: CommandContext): Promise<void> {
        const name = cc.args.shift();
        db.playlists.findOne({guildID: cc.guildID, name: name}, async (err, playlist) => {
            if(!playlist || err) return; //TODO: MSG err
            if(playlist.admins.indexOf(cc.member.id) == -1) return; //TODO: MSG err
            for (const url of cc.args) {
                const newSong = await PlAdd.getSong(url, cc);
                if(newSong != null)
                    db.playlists.update({ _id: playlist._id }, { $addToSet: { songs: newSong } });
            }
        });
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }
    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }

    private static async getSong(url: string, cc: CommandContext): Promise<Song|null> {
            if(!ytdl.validateURL(url)) {
                let results = (await search(url, opts)).results;
                results = results.filter(r => r.kind == "youtube#video")
                results = results.slice(0,5);
                let msg = "";
                results.forEach((item, index) => {
                    msg += `${index} : ${item.title}\n`;
                });
                let result = await QCM(msg, "YouTube Search:", cc.textChannel, cc.member);
                url = results[result].link;
            }
            const info = await ytdl.getInfo(url).catch(_reason => {});
            if(info)
                return ({
                    isAvailable: true,
                    title: info.videoDetails.title,
                    url: url,
                    volume: 0.5
                });
            else
                return null;
    }

}