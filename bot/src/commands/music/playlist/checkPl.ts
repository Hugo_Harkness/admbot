import {Command} from "../../command";
import {CommandContext} from "../../commandContext";
import {db} from "../../../db";
import {Song} from "../song";
import log from '../../../utils/log';
import ytdl from "ytdl-core";

export class CheckPl implements Command {
    commandNames = ["check", "check-playlist"];
    group = "Music:";

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let mod = cc.guildRoles.get("mod");
        return mod !== undefined && cc.roles.has(mod);
    }

    async run(cc: CommandContext): Promise<void> {
            for (const playlist of db.playlists.getAllData()) {
                const songs : Array<Song> = await Promise.all(playlist.songs.map(CheckPl.getSong));
                const newPlaylist = {
                    admins: playlist.admins,
                    name: playlist.name,
                    songs: songs,
                }
                db.playlists.update({_id: playlist._id}, newPlaylist);
                log(1, `Checked: ${playlist.name}`, `CheckPL`, cc.guild.name);
            }
    }

    private static async getSong(song: Song): Promise<Song> {
        const info = await ytdl.getInfo(song.url).catch(_reason => {});
        if(info)
            return {
                isAvailable: true,
                title: info.videoDetails.title,
                url: song.url,
                volume: song.volume
            };
        else
            return {
                isAvailable: false,
                title: song.title,
                url: song.url,
                volume: song.volume
            };
    }
}