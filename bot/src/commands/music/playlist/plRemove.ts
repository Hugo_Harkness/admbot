import {Command} from '../../command';
import {CommandContext} from '../../commandContext';
import {db} from "../../../db";

export class PlRemove implements Command {
    commandNames = ["plrm", "remove-from-playlist"];
    group = "Music:";

    async run(cc: CommandContext): Promise<void> {
        const name = cc.args.shift();
        const songIndex = cc.args.shift();
        db.playlists.findOne({guildID: cc.guildID, name: name}, async (err, playlist) => {
            if(!playlist || err) return; //TODO: MSG err
            if(playlist.admins.indexOf(cc.member.id) == -1) return; //TODO: MSG err
            playlist.songs.splice(songIndex, 1);
            db.playlists.update({ _id: playlist._id }, playlist);
        });
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }
    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }

}