import {Command} from "../command";
import {StreamCtrl} from "../../data/music/StreamCtrl";
import {CommandContext} from "../commandContext";
import {Vote} from "../../utils/dialog";

export class Stop implements Command {
    commandNames = ["stop"];
    group = "Music:";
    private static streamCtrl: StreamCtrl;

    constructor(streamCtrl: StreamCtrl) {
        Stop.streamCtrl = streamCtrl;
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }

    async run(cc: CommandContext): Promise<void> {
        await Vote(``, `Vote **STOP**`, cc.textChannel, result => {
            if (result.yes > 0 && result.no == 0)
                Stop.streamCtrl.stop(cc.guildID);
        });
    }

}