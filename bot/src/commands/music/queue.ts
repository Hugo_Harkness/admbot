import { Command } from '../command';
import { CommandContext } from '../commandContext';
import {MessageEmbed} from 'discord.js';
import {StreamCtrl} from "../../data/music/StreamCtrl";

export class Queue implements Command {
    commandNames = ["queue", "q"];
    group = "Music:";
    private static streamCtrl : StreamCtrl;

    constructor(streamCtrl: StreamCtrl) {
        Queue.streamCtrl = streamCtrl;
    }

    async run(cc: CommandContext): Promise<void> {
            let embed = new MessageEmbed()
            embed.setTitle(`Playing:`);
            embed.setDescription(`${(!Queue.streamCtrl.getNow(cc.guildID)?.isAvailable) ? ":x:" : ""} ${Queue.streamCtrl.getNow(cc.guildID)?.title}`);
            const queue = Queue.streamCtrl.getQueue(cc.guildID);
            if(queue && queue.length > 0) {
                let next = "";
                queue.forEach(song => {
                    next += `${(!song.isAvailable) ? ":x:" : ""} ${song.title}\n`
                })
                embed.addField("Queue:", next);
            }
            await cc.message.reply(embed).then(msg => {msg.delete({timeout:60*1000})});
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }
    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }

}