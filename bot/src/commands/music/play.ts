import { Command } from '../command';
import { CommandContext } from '../commandContext';
import ytdl from 'ytdl-core';
import {db} from "../../db";
import {StreamCtrl} from "../../data/music/StreamCtrl";
import {Song} from "./song";
import {readFileSync} from "fs";
import search, {YouTubeSearchOptions} from "youtube-search";
import {QCM} from "../../utils/dialog";

const opts: YouTubeSearchOptions = {
    maxResults: 10,
    key: readFileSync("data/apiGoogle").toString().trim()
};

export class Play implements Command {
    commandNames = ["play", "p"];
    group = "Music:";
    private static streamCtrl : StreamCtrl;

    constructor(streamCtrl: StreamCtrl) {
        Play.streamCtrl = streamCtrl;
    }

    async run(cc: CommandContext): Promise<void> {
        const playable = cc.args[0]
        if(ytdl.validateURL(playable)) {
            const info = await ytdl.getInfo(playable).catch(_reason => {});
            if(info) {
                Play.streamCtrl.getQueue(cc.guild.id)?.push({
                    isAvailable: true,
                    title: info.videoDetails.title,
                    url: playable,
                    volume: 0.5
                });
                await Play.launch(cc);
            }
        } else {
            db.playlists.findOne({guildID: cc.guildID, name: playable}, (err, playlist) => {
                if(err) return; //TODO: msg error
                if(playlist) {
                    let songs;
                    if (cc.args[1] == "-f") {
                        let start = 0;
                        if (cc.args[2]) start = parseInt(cc.args[2]);
                        songs = playlist.songs.slice(start, playlist.songs.length).concat(playlist.songs.slice(0, start));
                    } else if (cc.args[1] == "-r") {
                        songs = Play.shuffleSongs(playlist.songs);
                    } else {
                        songs = playlist.songs;
                    }

                    const queue = Play.streamCtrl.getQueue(cc.guild.id);
                    if (queue)
                        Play.streamCtrl.setQueue(queue.concat(songs), cc.guild.id);
                    //TODO: msg added to queue

                    Play.launch(cc);
                } else {
                    search(playable, opts, (err, results) => {
                        if(err || !results) return console.log(err);
                        let resultsVideo = results.filter(r => r.kind == "youtube#video")
                        let resultsVideoFive = resultsVideo.slice(0,5);
                        if(!resultsVideoFive) return;
                        let msg = "";
                        resultsVideoFive.forEach((item, index) => {
                            msg += `${index} : ${item.title}\n`;
                        });
                        QCM(msg,"YouTube Search:", cc.textChannel, cc.member, async result => {
                            if(result == -1) return;
                            const info = await ytdl.getInfo(resultsVideoFive[result].link).catch(_reason => {});
                            if(info) {
                                Play.streamCtrl.getQueue(cc.guildID)?.push({
                                    isAvailable: true,
                                    title: info.videoDetails.title,
                                    url: resultsVideoFive[result].link,
                                    volume: 0.5
                                });
                                await Play.launch(cc);
                            }
                        });
                    });
                }
            })
        }
    }

    private static shuffleSongs(array: Array<Song>) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }

    private static async launch(cc: CommandContext) {
        if (!Play.streamCtrl.getIsPlaying(cc.guildID) && cc.voiceChannel) {
            Play.streamCtrl.setIsPlaying(true, cc.guildID);
            Play.streamCtrl.addFormats(cc.guildID);
            await Play.streamCtrl.play(cc.voiceChannel, cc.guildID);
        }
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }
    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }

}