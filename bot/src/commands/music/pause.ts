import {Command} from "../command";
import {StreamCtrl} from "../../data/music/StreamCtrl";
import {CommandContext} from "../commandContext";

export class Pause implements Command {
    commandNames = ["pause"];
    group = "Music:";
    private static streamCtrl: StreamCtrl;

    constructor(streamCtrl: StreamCtrl) {
        Pause.streamCtrl = streamCtrl;
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }

    async run(cc: CommandContext): Promise<void> {
        //TODO: vote
        Pause.streamCtrl.pauseToggle(cc.guildID);
    }

}