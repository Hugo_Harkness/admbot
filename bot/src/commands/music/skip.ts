import {Command} from "../command";
import {StreamCtrl} from "../../data/music/StreamCtrl";
import {CommandContext} from "../commandContext";
import {Vote} from "../../utils/dialog";

export class Skip implements Command {
    commandNames = ["skip", "s"];
    group = "Music:";
    private static streamCtrl: StreamCtrl;

    constructor(streamCtrl: StreamCtrl) {
        Skip.streamCtrl = streamCtrl;
    }

    getHelpMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    getUseMessage(prefix: string): string {
        return `TODO ${this.commandNames}`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }

    async run(cc: CommandContext): Promise<void> {
        await Vote(`${Skip.streamCtrl.getNow(cc.guildID)?.title}`, `Vote **Skip**:`, cc.textChannel, result => {
            if (result.yes > result.no || result.yes + result.no == 0)
                Skip.streamCtrl.skip(cc.guildID);
        });
    }

}