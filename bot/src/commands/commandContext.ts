import {Message, Collection, Role, Guild, GuildMember, VoiceChannel, TextChannel} from 'discord.js';

export class CommandContext {
    readonly command: string;
    readonly args: string[];
    readonly message: Message;
    readonly prefix: string;

    readonly guild: Guild;
    readonly guildID: string;
    readonly guildRoles: Map<string, string>;
    
    readonly member: GuildMember;
    readonly roles: Collection<string, Role>;

    readonly voiceChannel: VoiceChannel | null;
    readonly textChannel: TextChannel;

    constructor(message: Message, prefix: string){
        let args = message.content.slice(prefix.length).trim().match(/(".*?"|[^"\s]+)/g);
        if(!args) args = [""];

        this.command = (args.shift() as string).toLocaleLowerCase();
        this.args = args;
        this.message = message;
        this.prefix = prefix;

        this.guild = this.message.guild as Guild;
        this.guildID = this.guild.id;
        this.guildRoles = new Map<string, string>(this.guild.roles.cache.map(r => [r.name, r.id]));

        this.member = message.member as GuildMember;
        this.roles = this.member.roles.cache;

        this.voiceChannel = this.member.voice.channel;
        this.textChannel = this.message.channel as TextChannel;
    }
}