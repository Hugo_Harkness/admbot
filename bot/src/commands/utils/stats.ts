import { Command } from '../command';
import { CommandContext } from '../commandContext';
import {exec} from 'child_process';

export class Stats implements Command {
    commandNames = ["stats"];
    group = "Util:";

    async run(cc: CommandContext): Promise<void> {
        exec('wc `find src -type f` | tail -n 1',
            function (error, stdout) {
                let result = stdout.match(/(".*?"|[^"\s]+)/g);
                if(result)
                    cc.message.reply(`${result[0]} Lines, ${result[1]} Words, ${result[2]} Characters.`).then(msg => msg.delete({timeout: 20*1000}));
            });
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}stats : Give useful stats`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}stats : Give useful stats.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }
}