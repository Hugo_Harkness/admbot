import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class Adm implements Command {
    commandNames = ["adm"];
    group = "Util:";

    async run(cc: CommandContext): Promise<void> {
        let adm = cc.guildRoles.get("adm");
        if(!adm) return;
        if(cc.roles.has(adm))
            cc.message.member?.roles.remove(adm);
        else
            cc.message.member?.roles.add(adm);
        
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}adm : Open all the doors (more).`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}adm : Open all the doors (more).`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return cc.message.author.id === "389432812266192906";
    }
}