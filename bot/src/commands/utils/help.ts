
import { MessageEmbed } from 'discord.js';
import log from '../../utils/log';
import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class Help implements Command {
    commandNames = [ "help" ];
    group = "Util:";
    grp = ["Util:", "Group:", "Private:", "Role:","Music:"];
    private commands: Command[];

    constructor(commands: Command[]){
        this.commands = commands;
    }

    async run(cc: CommandContext): Promise<void> {
        const allowedCommands = this.commands.filter(command => command.hasPermisionToRun(cc));
        
        if(cc.args.length == 0){
            let useEmbed = new MessageEmbed()
            useEmbed.setTitle("Available command:")
            this.grp.forEach(grp => {
                let acmds = allowedCommands.filter(cmd => {return cmd.group == grp;});
                if (acmds.length > 0){
                    let inGroupUseMessage = "" // text containing the use of all functions of the group
                    acmds.forEach(cmd => {
                        inGroupUseMessage += cmd.getUseMessage(cc.prefix) + "\n";
                    });
                    useEmbed.addField(grp,inGroupUseMessage)
                }
            })

            await cc.message.reply(useEmbed).then((msg) => msg.delete({ timeout: 60000 }));
            return;
        }

        const matchedCommands = allowedCommands.find(command => command.commandNames.includes(cc.args[0]));
        
        if (!matchedCommands) {
            await cc.message.reply('Help: unknown or not allowed command !').then((msg) => msg.delete({ timeout: 60000 }));
            log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (unknown)`,"Help", cc.guild.name);
        } else
            await cc.message.reply("\n\n" + matchedCommands.getHelpMessage(cc.prefix)).then((msg) => msg.delete({ timeout: 60000 }));

    }

    getHelpMessage(prefix: string): string {
        return prefix + "help [cmd] : Show list of commands or detailed help.";
    }
    getUseMessage(prefix: string): string {
        return prefix + "help [cmd] : Show list of commands or detailed help.";
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return true;
    }
}