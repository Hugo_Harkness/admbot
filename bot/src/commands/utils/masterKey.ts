import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class MasterKey implements Command {
    commandNames = ["masterkey"];
    group = "Util:";

    async run(cc: CommandContext): Promise<void> {
        let masterKey = cc.guildRoles.get("master_key");
        if(!masterKey) return;
        if(cc.roles.has(masterKey))
            cc.message.member?.roles.remove(masterKey);
        else
            cc.message.member?.roles.add(masterKey);
        
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}masterkey : Open all the doors.`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}masterkey : Open all the doors.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let mod = cc.guildRoles.get("mod");
        return mod !== undefined && cc.roles.has(mod);
    }
}