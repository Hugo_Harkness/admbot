import { CommandContext } from './commandContext';

export interface Command {
    readonly commandNames: string[];
    readonly group: string;
    run(commandContext: CommandContext): Promise<void>;
    getHelpMessage(prefix: string): string;
    getUseMessage(prefix: string): string;
    hasPermisionToRun(commandContext: CommandContext): boolean;
}