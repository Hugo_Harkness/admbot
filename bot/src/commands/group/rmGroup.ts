import { Command } from '../command';
import { CommandContext } from '../commandContext';
import { db } from '../../db';
import log from '../../utils/log';

export class RmGroup implements Command {
    commandNames = ["rmgrp", "remove-group"];
    group = "Group:";

    async run(cc: CommandContext): Promise<void> {
        let role = cc.message.mentions.roles.find(r => r.name.startsWith("grp-"));
        if(!role) {
            cc.message.reply(`${cc.args[0]} is not a group!`).then((msg) => msg.delete({ timeout: 60000 }));
            log(2, `${cc.member.user.tag} tried to issue >${cc.message.cleanContent}< (not a group)`,"RmGroup", cc.guild.name);
            return;
        }
        db.groups.findOne({ guildID: cc.guild.id, roleID: role.id }, async function (err, doc ) {
            if(err) return;
            if(doc.admin.find((adminID: string) => {return adminID == cc.member.id}) == undefined) {
                cc.message.reply(`operation forbidden, you are not admin of this group!`).then((msg) => msg.delete({ timeout: 60000 }));
                log(3, `${cc.member.user.tag} tried to issue >${cc.message.cleanContent}< (admin)`,"RmGroup", cc.guild.name);
                return;
            }

            cc.guild.channels.cache.filter(chan => chan.parentID == doc.catID).map(chan => chan.delete());
            role?.delete();
            cc.guild.channels.cache.get(doc.catID)?.delete();
            db.groups.remove({ _id: doc._id });
        });
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> : Remove a group and its channels.\nAlias: ${prefix}remove-group`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> : Remove a group and its channels.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }
}
