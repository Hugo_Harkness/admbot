import { db } from '../../db';
import { joinGrp } from '../../utils/GroupOp';
import log from '../../utils/log';
import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class JoinGroupe implements Command {
    commandNames = ["jgrp", "join-group"];
    group = "Group:";

    async run(cc: CommandContext): Promise<void> {
        let role = cc.message.mentions.roles.find(r => r.name.startsWith("grp-"));
        let members = cc.message.mentions.members?.array();
        if (cc.message.mentions.everyone && cc.member.roles.cache.has(cc.guildRoles.get("mod") as string)) {
            members = (await cc.guild.members.fetch()).array().filter(member => !member.user.bot);
            console.log(members)
        }
        if (!role) {
            cc.message.reply(`${cc.args[0]} is not a group!`).then((msg) => msg.delete({ timeout: 60000 }));
            return;
        }
        db.groups.findOne({ guildID: cc.guild.id, roleID: role.id }, async function (err, doc) {
            if (err) return;
            if (doc.private) {
                if (doc.admin.find((adminID: string) => { return adminID == cc.member.id }) == undefined) {
                    if (doc.members.find((memberID: string) => { return memberID == cc.member.id }) == undefined) {
                        cc.message.reply(`operation forbidden, this group is private!`).then((msg) => msg.delete({ timeout: 60000 }));
                        log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (grp private)`,"JoinGroupe", cc.guild.name);
                    }else{
                        cc.message.reply(`operation forbidden, you are not admin of this group!`).then((msg) => msg.delete({ timeout: 60000 }));
                        log(3, `${cc.member.user.tag} tried to issue >${cc.message.cleanContent}< (admin)`,"JoinGroupe", cc.guild.name);
                    }
                    return;
                }
                if (members?.length == 0)
                    cc.message.reply(`operation failed, missing user tag! (private group)`).then((msg) => msg.delete({ timeout: 60000 }));
                    log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (user tag)`,"JoinGroupe", cc.guild.name);
                members?.forEach((m) => {
                    joinGrp(doc, m, cc);
                });
            } else {
                if (members?.length == 0) {
                    if (!role) return;
                    joinGrp(doc, cc.member, cc);
                } else {
                    /*********modo**********/
                    if (cc.member.roles.cache.has(cc.guildRoles.get("mod") as string)) {
                        members?.forEach((m) => {
                            joinGrp(doc, m, cc);
                        });
                        return;
                    }
                    /***********************/
                    let tmpMem = members?.filter(m => m.roles.cache.has(cc.guildRoles.get("tmp") as string));
                    if (tmpMem?.length == 0) {
                        cc.message.reply(`operation failed, this group is public: they can join by themself.`).then((msg) => msg.delete({ timeout: 60000 }));
                        log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (grp public, not modo)`,"JoinGroupe", cc.guild.name);
                        return;
                    }
                    tmpMem?.forEach((m) => {
                        joinGrp(doc, m, cc);
                    });
                }
            }
        });
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> [@user]* : Join a group and its channels.\nAlias: ${prefix}join-group`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> [@user]* : Join a group and its channels.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let guest = cc.guildRoles.get("guest");
        return guest !== undefined && cc.roles.has(guest);
    }
}