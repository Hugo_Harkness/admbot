import { Command } from '../command';
import { CommandContext } from '../commandContext';
import { CategoryChannel, Guild, GuildMember, MessageEmbed, Permissions, Role } from 'discord.js';
import { db } from '../../db';
import { adminGrp } from '../../utils/GroupOp';
import log from '../../utils/log';

const filOri = __filename.slice(__dirname.length + 1);

export class admGroup implements Command {
    commandNames = ["admgrp", "adm-group"];
    group = "Group:";

    async run(cc: CommandContext): Promise<void> {
        let role = cc.message.mentions.roles.find(r => r.name.startsWith("grp-"));
        let member = cc.message.mentions.members?.first();
        if(!role) {
            cc.message.reply(`${cc.args[0]} is not a group!`).then((msg) => msg.delete({ timeout: 60000 }));
            log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (not a group)`,"admGroup", cc.guild.name);
            return;
        }
        if(member == undefined) {
            cc.message.reply(`operation failed, missing user tag!`).then((msg) => msg.delete({ timeout: 60000 }));
            log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (missing user tag)`,"admGroup", cc.guild.name);
            return;
        }
        
        db.groups.findOne({ guildID: cc.guild.id, roleID: role.id }, async function (err, doc ) {
            if(err) return;
            if(doc.members.find((memberID: string) => {return memberID == member?.id}) == undefined) {
                cc.message.reply(`operation forbidden, ${member?.nickname} is not a member of this group!`).then((msg) => msg.delete({ timeout: 60000 }));
                log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (${member?.nickname} is not a member)`,"admGroup", cc.guild.name);
                return;
            }
            if(doc.admin.find((adminID: string) => {return adminID == cc.member.id}) == undefined && !cc.roles.has(cc.guildRoles.get("mod") as string)) {
                cc.message.reply(`operation forbidden, you are not admin of this group!`).then((msg) => msg.delete({ timeout: 60000 }));
                log(3, `${cc.member.user.tag} tried to issue >${cc.message.cleanContent}< (not admin)`,"admGroup", cc.guild.name);
                return;
            }
            if(member)
                adminGrp(doc, member, cc);
        });
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> <@user> : give permisions to @user on @grp `;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> <@user> : give permisions to @user on @grp.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }
}