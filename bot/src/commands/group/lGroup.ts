import { Collection, GuildMember, Role } from 'discord.js';
import { db } from '../../db';
import { leaveGrp } from '../../utils/GroupOp';
import log from '../../utils/log';
import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class LeaveGroupe implements Command {
    commandNames = ["lgrp", "leave-group"];
    group = "Group:";

    async run(cc: CommandContext): Promise<void> {
        let role = cc.message.mentions.roles.find(r => r.name.startsWith("grp-"));
        let members = cc.message.mentions.members;
        if (!role) {
            cc.message.reply(`${cc.args[0]} is not a group!`).then((msg) => msg.delete({ timeout: 60000 }));
            return;
        }
        db.groups.findOne({ guildID: cc.guild.id, roleID: role.id }, async (err, doc) => {
            if (err) return;
            if (doc.private) {
                if(doc.members.length == 1) {
                    cc.message.reply(`operation forbidden, you are the last of this private group!\nif you are admin of this group you can use !!rmgrp, else contact a modo. `).then((msg) => msg.delete({ timeout: 60000 }));
                    log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (last of this private group)`,"LeaveGroupe", cc.guild.name);
                    return;
                }
                if (members?.size == 0) {
                    if (!role) return;
                    leaveGrp(doc, cc.member, cc);
                } else {
                    if (doc.admin.find((adminID: string) => { return adminID == cc.member.id }) == undefined) {
                        cc.message.reply(`operation forbidden, you are not admin of this group!`).then((msg) => msg.delete({ timeout: 60000 }));
                        log(3, `${cc.member.user.tag} tried to issue >${cc.message.cleanContent}< (admin)`,"LeaveGroupe", cc.guild.name);
                        return;
                    }
                    members?.forEach((m) => {
                        leaveGrp(doc, m, cc);
                    });
                }
            } else {
                if (members?.size == 0) {
                    if (!role) return;
                    leaveGrp(doc, cc.member, cc);
                } else {
                    /*********modo**********/
                    if (cc.member.roles.cache.has(cc.guildRoles.get("mod") as string)) {
                        members?.forEach((m) => {
                            leaveGrp(doc, m, cc);
                        });
                        return;
                    }
                    /***********************/
                    let tmpMem = members?.filter(m => m.roles.cache.has(cc.guildRoles.get("tmp") as string));
                    if (tmpMem?.size == 0) {
                        cc.message.reply(`operation failed, this group is public: they can leave by themself.`).then((msg) => msg.delete({ timeout: 60000 }));
                        log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (grp public, not modo)`,"LeaveGroupe", cc.guild.name);
                        return;
                    }
                    tmpMem?.forEach((m) => {
                        leaveGrp(doc, m, cc);
                    });
                }
            }
        });

        let tmpMem = cc.message.mentions.members?.find(m => m.roles.cache.has(cc.guildRoles.get("tmp") as string));
        if (tmpMem && role) tmpMem.roles.add(role);
        else if (role) cc.message.member?.roles.add(role);
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> [@user]* : Leave a group and its channels.\nAlias: ${prefix}join-group`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> [@user]* : Leave a group and its channels.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let guest = cc.guildRoles.get("guest");
        return guest !== undefined && cc.roles.has(guest);
    }
}