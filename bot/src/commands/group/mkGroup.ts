import { Command } from '../command';
import { CommandContext } from '../commandContext';
import { CategoryChannel, Guild, GuildMember, Permissions, Role } from 'discord.js';
import { db } from '../../db';
import log from '../../utils/log';

export class MkGroup implements Command {
    commandNames = ["mkgrp", "make-group"];
    group = "Group:";

    async run(cc: CommandContext): Promise<void> {
        if (!cc.args[0]) {
            cc.message.reply(`${cc.prefix}mkgrp <name>`).then((msg) => msg.delete({ timeout: 60000 }));
            log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (no name)`,"MkGroup", cc.guild.name);
            return;
        }
        const grpName = `grp-${cc.args[0]}`;
        let prv = false;
        if(cc.args[1] && cc.args[1] == "private") prv = true;

        if (cc.guildRoles.has(grpName)) {
            cc.message.reply(`${grpName} already exist!`).then((msg) => msg.delete({ timeout: 60000 }));
            log(2, `${cc.member.user.tag} failed to issue >${cc.message.cleanContent}< (already exist)`,"MkGroup", cc.guild.name);
            return;
        }

        genRoleGrp(cc.guild, grpName).then(grp => {
            let masterkey = cc.guildRoles.get("master_key");
            if (!masterkey) return;

            cc.guild.channels.create(grpName, { 
                type: "category",
                permissionOverwrites: [{ id: cc.guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: cc.member.id, allow: new Permissions(["VIEW_CHANNEL", "MANAGE_CHANNELS"]) }, { id: grp, allow: new Permissions("VIEW_CHANNEL") }, { id: masterkey, allow: new Permissions("VIEW_CHANNEL") }] 
            }).then(category => {
                if (!masterkey) return;
                genChannels(cc.guild, grpName, category, grp, cc.guild.roles.cache.get(masterkey) as Role);
                dbInsert(cc.guild, grp, category, cc.member, prv);
            });
                
            cc.member.roles.add(grp);
        })
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <name> [private]: Create new group and its channels.\nAlias: ${prefix}make-group`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <name> [private]: Create new group and its channels.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }
}

function genRoleGrp(guild: Guild, grpName: string): Promise<Role> {
    return guild.roles.create({
        data: {
            name: grpName,
            hoist: false,
            permissions: [],
            mentionable: true,
        },
        reason: "grp role."
    })
}

async function genChannels(guild: Guild, name: string, category: CategoryChannel, grp: Role, masterkey: Role) {
    guild.channels.create(`${name}-txt`, {
        type: "text",
        parent: category,
        //permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: grp, allow: new Permissions("VIEW_CHANNEL") }, { id: masterkey, allow: new Permissions("VIEW_CHANNEL") }]
    });
    guild.channels.create(`${name}-voc`, {
        type: "voice",
        parent: category,
        //permissionOverwrites: [{ id: guild.id, deny: new Permissions("VIEW_CHANNEL") }, { id: grp, allow: new Permissions("VIEW_CHANNEL") }, { id: masterkey, allow: new Permissions("VIEW_CHANNEL") }]
    });
}

function dbInsert(guild: Guild, role: Role, category: CategoryChannel, member: GuildMember, prv: Boolean) {
    db.groups.insert({
        guildID: guild.id,
        roleID: role.id,
        catID: category.id,
        private: prv,
        members: [member.id],
        admin: [member.id]
    });
}
