import { Command } from '../command';
import { CommandContext } from '../commandContext';
import { CategoryChannel, Guild, GuildMember, MessageEmbed, Permissions, Role } from 'discord.js';
import { db } from '../../db';

const filOri = __filename.slice(__dirname.length + 1);

export class LsGroup implements Command {
    commandNames = ["lsgrp", "list-group"];
    group = "Group:";

    async run(cc: CommandContext): Promise<void> {
        let embed = new MessageEmbed()
        embed.setTitle("All groups:")
        db.groups.find({ guildID: cc.guild.id, private: false }, async function (err: any, docs: { roleID: string; }[]) {
            if(err) return;
            let publicGrp = ""
            docs.forEach((doc: { roleID: string; }) => {
                publicGrp +=  cc.guild.roles.cache.get(doc.roleID)?.name + "\n";
            });
            if(publicGrp != "")
                embed.addField("Public:",publicGrp);

            db.groups.find({ guildID: cc.guild.id, private: true }, async function (err: any, docs: any) {
                if(err) return;
                let privateGrp = ""
                docs.forEach((doc: { roleID: string; }) => {
                    privateGrp +=  cc.guild.roles.cache.get(doc.roleID)?.name + "\n";
                });
                if(privateGrp != "")
                    embed.addField("Private:",privateGrp);

                await cc.message.reply(embed).then((msg) => msg.delete({ timeout: 60000 }));
            });
        });
       
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> : List all existing groups.`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}${this.commandNames[0]} <@grp> : List all existing groups.`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let guest = cc.guildRoles.get("guest");
        return guest !== undefined && cc.roles.has(guest);
    }
}