import {Message, Channel} from 'discord.js';
import {Command} from './command';
import {CommandContext} from './commandContext';
import {StreamCtrl} from "../data/music/StreamCtrl";
import log from '../utils/log';

//Commands:
import {Help} from './utils/help';
import {MasterKey} from './utils/masterKey';
import {Adm} from './utils/adm';

import {PromTG} from './role/promoteToGuest';
import {PromTM} from './role/promoteToMember';
import {PromTMod} from './role/promoteToMod';
import {Kick} from './role/kick';
import {DemTG} from './role/demoteToG';

import {MkGroup} from './group/mkGroup';
import {RmGroup} from './group/rmGroup';
import {JoinGroupe} from './group/JGroup';
import {LeaveGroupe} from './group/lGroup';
import {LsGroup} from './group/lsGroup';
import {admGroup} from './group/admGroup';
import {chModGroup} from './group/chModGroup';

import {Play} from "./music/play";
import {Skip} from "./music/skip";
import {Stop} from "./music/stop";
import {Queue} from "./music/queue";
import {Pause} from "./music/pause";

import {MkPl} from "./music/playlist/mkPl";
import {LsPl} from "./music/playlist/lsPl";
import {PlAdd} from "./music/playlist/plAdd";
import {PlRemove} from "./music/playlist/plRemove";
import {RmPl} from "./music/playlist/rmPl";
import {CheckPl} from "./music/playlist/checkPl";
import {Stats} from "./utils/stats";

export class CommandHandler {
    private readonly commands: Command[];
    private readonly prefix: string;

    constructor(prefix: string, streamCtrl: StreamCtrl) {
        const commandClasses = [DemTG, PromTG, PromTM, PromTMod, Kick, Adm, MasterKey, LsGroup, MkGroup, RmGroup,
            JoinGroupe, LeaveGroupe, admGroup, chModGroup, MkPl, LsPl, PlAdd, PlRemove, RmPl, CheckPl, Stats];
        const commandClassesStreamCtrl = [Play, Skip, Stop, Queue, Pause];

        this.commands = commandClasses.map(commandClass => new commandClass());
        let commandsStreamCtrl = commandClassesStreamCtrl.map(commandClass => new commandClass(streamCtrl));
        this.commands = this.commands.concat(commandsStreamCtrl);
        this.commands.push(new Help(this.commands));
        this.prefix = prefix;
    }

    async handleMessage(message: Message): Promise<void> {
        if (!this.isCommand(message)) return undefined;
        const commandContext = new CommandContext(message, this.prefix);

        const matchedCommands = this.commands.find(command => command.commandNames.includes(commandContext.command));

        if (!matchedCommands) {
            message.reply('Unknown command! try "' + this.prefix + 'help"').then((msg) => msg.delete({timeout: 60000}));
            log(2, `${message.author.tag} tried unknown command >${message.cleanContent}<`, "command", commandContext.guild.name);
        } else {
            if (matchedCommands.hasPermisionToRun(commandContext)) {
                log(0, `${message.author.tag} issued >${message.cleanContent}<`, "command", commandContext.guild.name);
                matchedCommands.run(commandContext);
            } else {
                log(3, `${message.author.tag} tried to issue >${message.cleanContent}<`, "command", commandContext.guild.name);
            }
        }
        await message.delete({timeout: 60000});
    }

    private isCommand(message: Message): boolean {
        return (message.guild !== null
            && message.channel.id === (message.guild.channels.cache.find(c => c.name === "bot") as Channel).id
            && message.content.startsWith(this.prefix)
            && !message.author.bot);
    }
}