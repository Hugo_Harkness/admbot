import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class PromTM implements Command {
    commandNames = ["ptm", "prom-to-member"];
    group = "Role:";

    async run(cc: CommandContext): Promise<void> {
        let members = cc.message.mentions.members;
        if(!members) return;
        members.map(memb => {
            memb.roles.add(cc.guildRoles.get("guest")as string);
            memb.roles.add(cc.guildRoles.get("member")as string);
            memb.roles.remove(cc.guildRoles.get("tmp")as string);
        })
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}ptm <@user_tag> : Promote a "tmp" or a "guest" to "member".\nAlias: ${prefix}prom-to-member`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}ptm <@user_tag> : Promote a "tmp" or a "guest" to "member".`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let mod = cc.guildRoles.get("mod");
        return mod !== undefined && cc.roles.has(mod);
    }
}