import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class Kick implements Command {
    commandNames = ["kick"];
    group = "Role:";

    async run(cc: CommandContext): Promise<void> {

        let members = cc.message.mentions.members;
        if (!members) return;

        let mod = cc.guildRoles.get("mod");
        if (!mod) return;

        if (cc.roles.has(mod))
            members.map(memb => {
                let member = cc.guildRoles.get("member");
                if (member && !memb.roles.cache.has(member))
                    memb.kick();
            })
        else
            members.map(memb => {
                let tmp = cc.guildRoles.get("tmp");
                if (tmp && memb.roles.cache.has(tmp))
                    memb.kick();
            })
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}kick <@user_tag> : kick user (at least 2 rank below yours.)`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}kick <@user_tag> : kick user (at least 2 rank below yours.)`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }
}