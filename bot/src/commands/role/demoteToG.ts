import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class DemTG implements Command {
    commandNames = ["dtg", "dem-to-guest"];
    group = "Role:";

    async run(commandContext: CommandContext): Promise<void> {
        commandContext.message.delete({ timeout: 2000 }); 
        commandContext.message.mentions.members?.map(memb => {
            memb.roles.add(commandContext.guildRoles.get("guest")as string);
            memb.roles.remove(commandContext.guildRoles.get("member")as string);
            memb.roles.remove(commandContext.guildRoles.get("mod")as string);
            memb.roles.remove(commandContext.guildRoles.get("tmp")as string);
        })
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}dtg <@user_tag> : demote to "guest".\nAlias: ${prefix}dem-to-guest`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}dtg <@user_tag> : demote to "guest".`;
    }

    hasPermisionToRun(commandContext: CommandContext): boolean {
        return commandContext.message.author.id === "389432812266192906";
    }
}