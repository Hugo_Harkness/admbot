import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class PromTMod implements Command {
    commandNames = ["ptmod", "prom-to-mod"];
    group = "Role:";

    async run(cc: CommandContext): Promise<void> {
        let members = cc.message.mentions.members;
        if(!members) return;
        members.map(memb => {
            memb.roles.add(cc.guildRoles.get("guest")as string);
            memb.roles.add(cc.guildRoles.get("member")as string);
            memb.roles.add(cc.guildRoles.get("mod")as string);
            memb.roles.remove(cc.guildRoles.get("tmp")as string);
        })
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}ptmod <@user_tag> : Promote "mod".\nAlias: ${prefix}prom-to-mod`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}ptmod <@user_tag> : Promote "mod".`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        return cc.message.author.id === "389432812266192906";
    }
}