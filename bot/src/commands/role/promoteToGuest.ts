import { Command } from '../command';
import { CommandContext } from '../commandContext';

export class PromTG implements Command {
    commandNames = ["ptg", "prom-to-guest"];
    group = "Role:";

    async run(cc: CommandContext): Promise<void> {
        let members = cc.message.mentions.members;
        if(!members) return;
        members.map(memb => {
            memb.roles.add(cc.guildRoles.get("guest")as string);
            memb.roles.remove(cc.guildRoles.get("tmp")as string);
        })
    }

    getHelpMessage(prefix: string): string {
        return `${prefix}ptg <@user_tag> : Promote a "tmp" to "guest".\nAlias: ${prefix}prom-to-guest`;
    }
    getUseMessage(prefix: string): string {
        return `${prefix}ptg <@user_tag> : Promote a "tmp" to "guest".`;
    }

    hasPermisionToRun(cc: CommandContext): boolean {
        let member = cc.guildRoles.get("member");
        return member !== undefined && cc.roles.has(member);
    }
}