FROM node:alpine
WORKDIR /app
RUN apk add --no-cache --virtual .build-deps make autoconf automake g++ libtool python3 tzdata
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime && echo Europe/Paris > /etc/timezone

COPY bot/package.json .
COPY bot/package-lock.json .
RUN npm install
RUN apk del .build-deps

COPY bot/tsconfig.json .
COPY bot/src/ ./src/
RUN npx tsc

CMD node bin/index.js